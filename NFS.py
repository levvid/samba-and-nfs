#NFS.py
#Gibson
#June 11, 2014
import HTML
import os

"""Module for extracting data from a file (smb.conf) and converting it to a table in html"""
def createTable():
    outfile = 'NFS.html'
    tableData = []
    errors = []
    nfs = nfsCommand()
    cdat = cdatCommand()
    for i in range(len(nfs)):
        if(i == 0):
            nfs[0] = 'users/'
        print nfs[i]
        try:
            data = '/nfs/'+nfs[i] + '!!' + makeString(runCommand('/nfs/'+nfs[i]))
        except:
            e = '/nfs/' + nfs[i] + '!!'
            errors.append(e.split('!!'))
            continue
        tableData.append(data.split('!!'))
    for line in cdat:
        try:
             data = '/cdat/'+line + '!!' + makeString(runCommand('/cdat/'+line))
        except:
            e = '/cdat/' + line + '!!'
            errors.append(e.split('!!'))
            continue
        tableData.append(data.split('!!'))
    
    ht = HTML.table(tableData,header_row=['LINUX PATH', 'FILESYSTEM', 'TOTAL SIZE', 'AMOUNT USED' , 'AMOUNT FREE', 'PERCENT USED'])
    #remove spare column at the end            
    for i in range(len(ht)):
        ht = ht.replace('<TD>&nbsp;</TD>','')
    ins = ht.find("<TABLE")
    notFound = HTML.table(errors,header_row=['NOT FOUND'])
    for i in range(len(notFound)):
        notFound = notFound.replace('<TD>&nbsp;</TD>','')
        
    htmlcode = head + ht[0:ins+6]+ sort + ht[(ins+6):] + end + notFound+end2
    
    
    with open(outfile, 'w') as myFile:     #create a html file called samba and save it
        myFile.write(htmlcode)
    print htmlcode
    
    
    
    


def nfsCommand():
    """runs the "ypcat -k amd.nfs |awk '{print $1}'" command and returns the values"""
    p = os.popen("ypcat -k amd.nfs |awk '{print $1}'")    #run the command
    s = p.readlines()   #take the output from cmd and convert it to a string array line by line
    return s    #return the cmd output


def cdatCommand():
    """runs the "ypcat -k amd.cdat |awk '{print $1}'" command and returns the values"""
    p = os.popen("ypcat -k amd.cdat |awk '{print $1}'")    #run the command
    s = p.readlines()   #take the output from cmd and convert it to a string array line by line
    return s    #return the cmd output


def runCommand(path):
    """Takes the path and runs the 'df -h' command on it and returns the second line
    of the output from cmd"""
    p = os.popen('df -h '+ path)    #run the command
    s = p.readlines()   #take the output from cmd and convert it to a string array line by line
    return s[1]     #return the second line


def makeString(s):
    """takes input from runCommand() and splits it into a string separating each value by '!!'
    for easier splitting"""
    table2 = ""     #string to hold the values as they are being sliced from s
    tmp = s     #temporary variable
    index = s.find(" ")     #find first space
    while (index!=-1):  #so long as the string has a space in it
        data = tmp[0:index]
        tmp = tmp[index:].strip()
        table2 = table2+data + "!!"
        index = tmp.find(" ")
    return table2




style = ' <style> table.sortable th:not(.sorttable_sorted):not(.sorttable_sorted_reverse):not(.sorttable_nosort):after{content: " \\25B4\\25BE" }p{ font-family:Verdana, Geneva, sans-serif; font-size:.9em;text-align:justify;}th:not(.sorttable_nosort){cursor:pointer; text-decoration: underline;}h1{ font-family:"Trebuchet MS", Arial, Helvetica, sans-serif; font-size:1.4em;text-align:left;}td,th {border:1px solid #98bf21;padding:3px 7px 2px 7px;}th, head{font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;font-size:1.4em; text-align:center; padding-top:5px; padding-bottom:4px; background-color:#A7C942; color:#fff;}tr.alt td {color:#000;background-color:#EAF2D3;}</style>'
title = '<title>CLASSE NFS Directories</title><h1>CLASSE NFS Directories</h1>'
heading = '<p>Please see below for a listing of CLASSE file systems accessible over Samba and NFS.<br>If you would like assistance determining where to store your data or would like a new file system created, please email <a href="mailto:service-classe@cornell.edu">service-classe@cornell.edu</a><br>For more on data stewardship at CLASSE, please see <a href="https://wiki.classe.cornell.edu/Computing/DataStewardship">https://wiki.classe.cornell.edu/Computing/DataStewardship</a></p>'
head = '<!DOCTYPE html><html><head><script src="sortTable.js"></script>' +title+ heading+style + '</head><body>'

sort = ' style="background-color:white;" class="sortable" '
end = '</TABLE>'
end2 = '</TABLE></body></html>'



if __name__ == "__main__":
    createTable()
    