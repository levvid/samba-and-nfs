#samba.py
#Gibson
#June 06, 2014
import HTML
import os
import collections

"""Module for extracting data from a file (smb.conf) and converting it to a table in html"""
def createTable():
    """extracts the data and converts it to table in html"""
    infile = "/home/glm83/Downloads/smb.conf"
    outfile ="samba.html"
    #infile = "/etc/cluster/samba/samba:samba/smb.conf"
    #outfile = "/nfs/classe/www/html/private/computing/samba.html"
    text = open(infile)
    tableData = []     #list to hold table data for the html file
    sortDict = {}
    errors = []
    for line in text:   #loop through the entire file line by line 
        first = line.find("[")  #find first brace
        last = line.find("]")   #find last brace
        comment = line.find("comment = ")   #index of comment
        path = line.find("path = ") #index of path
        if(first!=-1):
            row = "\\\samba.classe.cornell.edu\\" + line[first+1:last]  #windows path
            mac = "!!"+"cifs://samba.classe.cornell.edu/" + line[first+1:last]  #mac os x path
            row = row+mac #windows path concatenated with mac os x path
        if(comment!=-1):
            row = line[comment+10:]  + "!!" +row  #comment
        if(path!=-1):
            row=row+"!!"+line[path+7:] #linux path
            try:
                s = makeString(runCommand(line[path+7:]))   #get the fileSystem, size, amount used....
                sortDict.update({line[path+7:]:(row+'!!'+s)})
                #tableData.append(row.split("!!") + s.split("!!"))   #take the row and filesystem data and append to a row in the table array
            except:
                e = line[path+7]+'!!'
                errors.append(e.split('!!'))
            #s = makeString(runCommand(line[path+7:]))   #get the fileSystem, size, amount used....
            #tableData.append(row.split("!!") + s.split("!!"))   #take the row and filesystem data and append to a row in the table array
    sortedDict = collections.OrderedDict(sorted(sortDict.items()))
    for i in range(len(sortedDict)):
        tableData.append(sortedDict.values()[i].split('!!'))
        
        
    t = HTML.table(tableData,header_row=['COMMENT','WINDOWS PATH', 'MAC OS X PATH','LINUX PATH',
                                                'FILESYSTEM','TOTAL SIZE', 'AMOUNT USED', 'AMOUNT FREE',
                                                'PERCENT USED'],)  # a header row for the table, in bold
    ht = str(t)
    
    column1 = ht.find("COMMENT<")
    ht = ht[0:column1-1]+' class="sorttable_alpha"'+ht[(column1-1):]
    for i in range(len(ht)):
        stripes = ht.find('<TR>')
        if(stripes!=-1): 
            ht = ht[0:stripes+3] + ' class="alt"'+ht[(stripes+3):]
     #remove spare column at the end            
    for i in range(len(ht)):
        ht = ht.replace('<TD>&nbsp;</TD>','')
        
    ins = ht.find("<TABLE")    
        
    htmlcode = head + ht[0:ins+6]+ sort + ht[(ins+6):] + end
    
    
    with open(outfile, 'w') as myFile:     #create a html file called samba and save it
        myFile.write(htmlcode)



def runCommand(path):
    """Takes the path and runs the 'df -h' command on it and returns the second line
    of the output from cmd"""
    p = os.popen('df -h '+ path)    #run the command
    s = p.readlines()   #take the output from cmd and convert it to a string array line by line
    return s[1]     #return the second line


def makeString(s):
    """takes input from runCommand() and splits it into a string separating each value by '!!'
    for easier splitting"""
    table2 = ""     #string to hold the values as they are being sliced from s
    tmp = s     #temporary variable
    index = s.find(" ")     #find first space
    while (index!=-1):  #so long as the string has a space in it
        data = tmp[0:index]
        tmp = tmp[index:].strip()
        table2 = table2+data + "!!"
        index = tmp.find(" ")
    return table2
    
 
style = ' <style> table.sortable th:not(.sorttable_sorted):not(.sorttable_sorted_reverse):not(.sorttable_nosort):after{content: " \\25B4\\25BE" }p{ font-family:Verdana, Geneva, sans-serif; font-size:.9em;text-align:justify;}th:not(.sorttable_nosort){cursor:pointer; text-decoration: underline;}h1{ font-family:"Trebuchet MS", Arial, Helvetica, sans-serif; font-size:1.4em;text-align:left;}td,th {border:1px solid #98bf21;padding:3px 7px 2px 7px;}th, head{font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;font-size:1.4em; text-align:center; padding-top:5px; padding-bottom:4px; background-color:#A7C942; color:#fff;}tr.alt td {color:#000;background-color:#EAF2D3;}</style>'
title = '<title>CLASSE Samba Directories</title><h1>CLASSE Samba Directories</h1>'
heading = '<p>Please see below for a listing of CLASSE file systems accessible over Samba and NFS.<br>If you would like assistance determining where to store your data or would like a new file system created, please email <a href="mailto:service-classe@cornell.edu">service-classe@cornell.edu</a><br>For more on data stewardship at CLASSE, please see <a href="https://wiki.classe.cornell.edu/Computing/DataStewardship">https://wiki.classe.cornell.edu/Computing/DataStewardship</a></p>'
head = '<!DOCTYPE html><html><head><script src="sortTable.js"></script>' +title+ heading+style + '</head><body>'

sort = ' style="background-color:white;" class="sortable" '
end = '</TABLE></body></html>'





   

if __name__ == "__main__":
    createTable()
    
    
